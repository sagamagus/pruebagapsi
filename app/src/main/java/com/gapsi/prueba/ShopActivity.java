package com.gapsi.prueba;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.gapsi.prueba.adapters.ProductsAdapter;
import com.gapsi.prueba.utils.Preferences;
import com.gapsi.prueba.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ShopActivity extends AppCompatActivity implements WebBridge.WebBridgeListener {

    RecyclerView list;
    EditText edtSearch, edtQty;
    TextView txtPage;
    String search;
    Spinner spSearches;
    int pagina, cantidad, total;
    List<String> spinnerArray =  new ArrayList<String>();
    JSONArray busquedas=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        list = findViewById(R.id.recycler_list);
        list.setLayoutManager(new GridLayoutManager(this, 2));
        edtSearch = findViewById(R.id.edt_search);
        spSearches = findViewById(R.id.sp_search);
        edtQty = findViewById(R.id.edt_qty);
        txtPage = findViewById(R.id.txt_page);
        pagina = 1;
        cantidad = 10;
        String qty = cantidad+"";
        edtQty.setText(qty);
        total = 0;
        if (Preferences.has("search",this)){
            try {
                String js = Preferences.get("search",this);
                Log.e("JSON:",js);
                busquedas = new JSONArray(js);
                for (int i = 0; i<busquedas.length();i++){
                    JSONObject act = busquedas.getJSONObject(i);
                    spinnerArray.add(act.getString("busqueda"));
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                            this, R.layout.spinner, spinnerArray);
                    adapter.setDropDownViewResource(R.layout.spinner_drop);
                    spSearches.setAdapter(adapter);
                    spSearches.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {
                            edtSearch.setText(spinnerArray.get(myPosition));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {

                        }

                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            spSearches.setVisibility(View.GONE);
        }
    }

    public void loadSearches(){
        try {
            if (busquedas!=null){
                spSearches.setVisibility(View.VISIBLE);
            }
            spinnerArray.clear();
            for (int i = 0; i<busquedas.length();i++){
                JSONObject act = busquedas.getJSONObject(i);
                spinnerArray.add(act.getString("busqueda"));
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                        this, R.layout.spinner, spinnerArray);
                adapter.setDropDownViewResource(R.layout.spinner_drop);
                spSearches.setAdapter(adapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void search(View view){
        search = edtSearch.getText().toString();
        if (search.equals("")){
            new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(R.string.error_box).setNeutralButton(R.string.btn_close, null).show();
        }else {
            String url = "";
            if (edtQty.getText().length()>0){
                cantidad = Integer.parseInt(edtQty.getText().toString());
            }
            url = "force-plp=true&search-string=" + search + "&page-number=" + pagina + "&number-of-items-per-page=" + cantidad;
            //+"&refinement=irpar"
            WebBridge.send(url, "Enviando", this, this);
        }
    }

    public void previus(View view){
        if (pagina>1) {
            pagina--;
        }
        search(view);
    }

    public void next(View view){
        if (pagina<total) {
            pagina++;
        }
        search(view);
    }

    public void deleteSearchs(View view){
        new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(R.string.txt_confirm).setNegativeButton(R.string.txt_no, null)
                .setPositiveButton(R.string.txt_si, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Preferences.clear(ShopActivity.this);
                        busquedas= null;
                        spSearches.setVisibility(View.GONE);
                        edtSearch.setText("");
                    }
                }).show();
    }

    @Override
    public void onWebBridgeSuccess(String url, String response) {

    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        Log.e("Entro:","succes");
        try {
            long tt = json.getJSONObject("plpResults").getJSONObject("plpState").getLong("totalNumRecs");
            total = (int)Math.ceil(tt/cantidad);
            String pg = pagina+"/"+total;
            txtPage.setText(pg);
            boolean add = true;
            if (busquedas!=null) {
                for (int i = 0; i < busquedas.length(); i++) {
                    if (busquedas.getJSONObject(i).getString("busqueda").equals(search)) {
                        add = false;
                        break;
                    }
                }
            }else{
                busquedas = new JSONArray();
                add = true;
            }
            if (add){
                if (!search.equals("")) {
                    JSONObject addSeacrh = new JSONObject();
                    addSeacrh.put("busqueda", search);
                    busquedas.put(addSeacrh);
                }
            }
            if (busquedas!=null){
                if (busquedas.length()>0) {
                    Preferences.set("search", busquedas.toString(), this);
                }
            }
            loadSearches();
            Log.e("Entro:","guardo prefs");
            JSONArray products = json.getJSONObject("plpResults").getJSONArray("records");
            RecyclerView.Adapter rvAdapter = new ProductsAdapter(products, this, this);
            list.setAdapter(rvAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {

    }
}
