package com.gapsi.prueba.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.gapsi.prueba.R;
import com.gapsi.prueba.ShopActivity;

import org.json.JSONArray;
import org.json.JSONException;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductViewHolder> {

    JSONArray data;
    Activity mActivity;
    Context mContext;

    public class ProductViewHolder extends RecyclerView.ViewHolder{

        String img, title, price, offer, location;

        ImageView foto;
        TextView txtTitle, txtPrice, txtOffer, txtLocation;


        public ProductViewHolder(View itemView) {
            super(itemView);

            foto = itemView.findViewById(R.id.img_foto);
            txtTitle = itemView.findViewById(R.id.txt_title);
            txtPrice = itemView.findViewById(R.id.txt_price);
            txtOffer = itemView.findViewById(R.id.txt_price_off);
            txtLocation = itemView.findViewById(R.id.txt_locatio);
        }

    }

    public ProductsAdapter(JSONArray data, Activity activity, Context context){
        Log.e("entro al adapter ","hay datos");
        this.data = data;
        mActivity = activity;
        mContext = context;

    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View weekView;
        weekView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_product, parent, false);
        return new ProductViewHolder(weekView);
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position) {

        try {
            Log.e("Entro:","elemento"+position);
            holder.title = data.getJSONObject(position).getString("productDisplayName");
            holder.price = data.getJSONObject(position).getString("listPrice");
            holder.offer = data.getJSONObject(position).getString("promoPrice");
            holder.location = data.getJSONObject(position).getString("productType");
            holder.img = data.getJSONObject(position).getString("smImage");
            Glide.with(mContext).load(holder.img).into(holder.foto);
            holder.txtTitle.setText(holder.title);
            holder.txtPrice.setText(holder.price);
            holder.txtOffer.setText(holder.offer);
            holder.txtLocation.setText(holder.location);
            if (holder.offer.equals("0.0")){
                holder.txtOffer.setVisibility(View.GONE);
            }else{
                holder.txtPrice.setPaintFlags(holder.txtPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }

        } catch (JSONException e) {

            new AlertDialog.Builder(mActivity).setTitle(R.string.txt_error).setMessage(R.string.error_connectivity).setNeutralButton(R.string.btn_close, null).show();

            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

}
