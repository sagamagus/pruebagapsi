package com.gapsi.prueba.utils;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


import java.util.Map;

public class Preferences {

    static public void set(String key, String value, Activity a) {
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(a).edit();
        e.putString("profile_" + key, value);
        e.commit();
    }

    static public String get(String key, Activity a) {

        Map<String,?> data = PreferenceManager.getDefaultSharedPreferences(a).getAll();
        Object result = data.get("profile_" + key);
        return result != null ? result.toString() : "";

    }

    static public boolean has(String key, Activity a) {

        Map<String,?> data1 = PreferenceManager.getDefaultSharedPreferences(a).getAll();
        return data1.containsKey("profile_" + key);

    }

    static public void clear(Activity a) {

        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(a).edit();
        Map<String,?> data = PreferenceManager.getDefaultSharedPreferences(a).getAll();

        e.clear();
        e.apply();

    }

}
