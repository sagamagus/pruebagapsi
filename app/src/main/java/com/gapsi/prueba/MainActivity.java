package com.gapsi.prueba;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(1000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{

                    /*if (User.getToken(SplashActivity.this) != null) {

                        if (User.get("puesto",SplashActivity.this).equals("tutor")) {
                            Intent homeIntent = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(homeIntent);
                        }else{
                            Intent homeIntent = new Intent(SplashActivity.this, HomeBecarioActivity.class);
                            startActivity(homeIntent);
                        }

                    } else {

                        Intent loginIntent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(loginIntent);

                    }*/

                    Intent loginIntent = new Intent(MainActivity.this, ShopActivity.class);
                    startActivity(loginIntent);

                    finish();

                }
            }
        };

        timerThread.start();
    }
}
